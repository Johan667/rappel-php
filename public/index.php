<?php

define('TEMPLATE_DIR', dirname(__DIR__) . '/templates');

if (!isset($_GET['page'])) {
    require_once TEMPLATE_DIR . '/home.php';
    die;
}


switch ($_GET['page']) {
    case 'article':
        require_once TEMPLATE_DIR . '/article.php';
        break;
    case 'contact':
        require_once TEMPLATE_DIR . '/contact.php';
        break;
    default:
        require_once TEMPLATE_DIR . '/home.php';
        break;
}