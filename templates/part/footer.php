
<footer>
    <div class="container">
        <form action="">
            <fieldset>
                <div>
                    <legend>Register now so you don't miss our programs</legend>
                </div>
                <div>
                    <input type="email" placeholder="Enter your Email">
                    <input type="submit" value="Subscribe now" class="heartbeat">
                </div>
            </fieldset>
        </form>

        <div id="footer-nav">
            <nav>
                <ul>
                    <li>Home</li>
                    <li>Category</li>
                    <li>About</li>
                    <li>Contact</li>
                </ul>
            </nav>
            <ul>
                <li><i class="fa-brands fa-facebook"></i></li>
                <li><i class="fa-brands fa-twitter"></i></li>
                <li><i class="fa-brands fa-instagram"></i></li>
                <li><i class="fa-brands fa-youtube"></i></li>
            </ul>
        </div>

        <hr>

        <img src="/img/logo.png" alt="logo">
        <div class="slide-in-right">Made By <strong>Johan</strong></div>
    </div>
</footer>

</body>
</html>