<?php
require('../templates/part/header.php');
require('../Model/dbconnect.php');

$articles = $mysqli->query("SELECT * FROM article");

?>

<section>
    <div class="container">
        <div class="section-title">
            <div>
                <span>Whats new ?</span>
                <h3>Take a look At some of our Pets</h3>
            </div>
            <button class="btn">View more</button>
        </div>

        <div class="product-grid">

            <?php
            foreach ($articles as $value){ ?>
                <article class="card">
                    <img src="/img/image_1.jpg" alt="chien">
                    <h4><?= $value['name']; ?></h4>
                    <dl>
                        <dt>Genre</dt>
                        <dd><?= $value['genre'] ?></dd>
                        <dt>Age</dt>
                        <?php  $age = date_diff(new DateTime($value['birthday']), new DateTime('now')); ?>
                        <dd><?= $age->y.'ans&nbsp;'.$age->m.'mois' ?></dd>
                    </dl>
                    <strong><?= ($value['price'] / 1000) ?>€</strong>
                    <strong><a href="templates/article.php?id=<?=$value['id']?>">VOIR</a></strong>
                </article>
            <?php   }
            ?>

        </div>

    </div>
</section>
<?php
include('../templates/part/footer.php');
?>
