<?php

require('templates/part/header.php');

require('Model/dbconnect.php');

if($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = $_GET['id'];

    $articles = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
    if ($id === null) {
        $id = 1;
    }
    $articles = $mysqli->query("SELECT * FROM article where id= '$id'");

    if ($articles === false) {
        echo "Aucun article";
    }
}

?>

<div class="product-grid">
    <?php
foreach ($articles as $value){ ?>
    <article class="card">
        <h1><?= $value['name']; ?></h1>
        <img src="public/img/image_1.jpg" alt="chien">

        <dl>
            <dt>Genre</dt>
            <dd><?= $value['genre'] ?></dd>
            <dt>Age</dt>
            <?php  $age = date_diff(new DateTime($value['birthday']), new DateTime('now')); ?>
            <dd><?= $age->y.'ans&nbsp;'.$age->m.'mois' ?></dd>
        </dl>
        <strong><?= ($value['price'] / 1000) ?>€</strong>

    </article>
<?php   }

?>
</div>
<?php
include('templates/part/footer.php');
?>
